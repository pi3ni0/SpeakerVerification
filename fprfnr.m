x = 0:0.01:1;
y1 = zeros(1, 101);
y2 = zeros(1, 101);
for i = 1:length(x)
    y1(i) = sum(FNR > x(i)) / length(FPR);
    y2(i) = sum(FPR < x(i)) / length(FPR);
end

plot(x, y1)
hold on
plot(x, y2)
xlabel('Threshold') 
ylabel('Prediction')