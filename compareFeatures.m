clear all
close all
%
%% Parameters
sharedParameters

%% FeaturesRBM

pathFeatures1 = fullfile(pathRoot, 'TIMIT/Features_AUTOENKODER');
paths = rdir(pathFeatures1, '*.HTK', 1);
nFiles = size(paths, 1);
data1 = cell(nFiles, 1);
for i = 1:nFiles,
    data1{i} = htkread(paths{i});
end
data1 = cat(2, data1{:})';

%% FeaturesMFCC

pathFeatures2 = fullfile(pathRoot, 'TIMIT/Features_MFCC');
paths = rdir(pathFeatures2, '*.HTK', 1);
nFiles = size(paths, 1);
data2 = cell(nFiles, 1);
for i = 1:nFiles,
    data2{i} = htkread(paths{i});
end
data2 = cat(2, data2{:})';

%% 
m = size(data1,1 ); 
cosinuses = zeros(m, 1);
for i = 1:m
   a = data1(i, :);
   b = data2(i, :);
   cosinuses(i) = a*b' / (norm(a)*norm(b));
end
